import * as express from "express";
const app = express();

import * as mongodb from "./utils/mongodb";

const port = 5000;

import appRoutes from "./routes";

app.use(express.json({limit: "50mb"}));

// Rutas base
const routes = [
  appRoutes
]

// Se cargan las rutas
app.use('/', routes);

// Escucha en el puerto 5000
app.listen(port, async () => {

  // Inicializa la conexion con la base de datos
  const result = await mongodb.start();

  if (result === true) {
    console.log("index.ts (listen): base de datos inicializada correctamente");
  }

  console.log(`Servidor inicializado en el puerto 5000.  Accede desde la URL http://localhost:5000`);
});

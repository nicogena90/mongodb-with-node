import { Request, Response } from "express";

import * as httpResp from "./httpResponses";
import * as mongodb from "../utils/mongodb";

const FILE_LOG = "controllers/data.ts";

/**
 * Función para traer los datos de la coleccion
 *
 * @param req   Request
 * @param res   Response
 */
export async function get(req: Request, res: Response) {
  
  try {

    const { collection } = req.params;

    if (collection) {

      const result = await mongodb.select(collection);

      // Retorno lista de personajes
      httpResp.success(res, result);

    } else {
      httpResp.error(res, app.httpResponses.errorCode.BAD_REQUEST, app.httpResponses.errorMsgs.MISSING_FIELDS);
    }

  } catch (error) {
    httpResp.error(res, app.httpResponses.errorCode.INTERNAL_SERVER_ERROR,
      `${FILE_LOG} (get): error (${error.stack})`);
  }
}

/**
 * Función para traer los datos segun id
 *
 * @param req   Request
 * @param res   Response
 */
export async function getById(req: Request, res: Response) {
  
  try {

    const { collection, id } = req.params;

    if (collection && id) {

      const query = { id: Number(id) };

      const result = await mongodb.select(collection, query);

      // Retorno lista de personajes
      httpResp.success(res, result);

    } else {
      console.log(`Parametros coleccion y valor incompletos`);
      httpResp.error(res, app.httpResponses.errorCode.BAD_REQUEST, app.httpResponses.errorMsgs.MISSING_FIELDS);
    }

  } catch (error) {
    httpResp.error(res, app.httpResponses.errorCode.INTERNAL_SERVER_ERROR,
      `${FILE_LOG} (getById): error (${error.stack})`);
  }
}

/**
 * Función para agregar items a la coleccion
 *
 * @param req   Request
 * @param res   Response
 */
export async function add(req: Request, res: Response) {
  
  try {

    const { collection, value } = req.body;

    if (collection && value) {

      // Ingreso nuevos valores en la base
      const result = await mongodb.insert(collection, value);

      httpResp.success(res, result);
    } else {
      console.log(`Parametros coleccion y valor incompletos`);
      httpResp.error(res, app.httpResponses.errorCode.BAD_REQUEST, app.httpResponses.errorMsgs.MISSING_FIELDS);
    }

  } catch (error) {
    httpResp.error(res, app.httpResponses.errorCode.INTERNAL_SERVER_ERROR,
      `${FILE_LOG} (add): error (${error.stack})`);
  }
}

/**
 * Función para eliminar items de una coleccion
 *
 * @param req   Request
 * @param res   Response
 */
export async function deleteValues(req: Request, res: Response) {
  
  try {

    const { collection, value } = req.body;

    if (collection && value) {

      // Ingreso nuevos valores en la base
      const result = await mongodb.deleteValues(collection, value);

      httpResp.success(res, result);
    } else {
      console.log(`Parametros coleccion y valor incompletos`);
      httpResp.error(res, app.httpResponses.errorCode.BAD_REQUEST, app.httpResponses.errorMsgs.MISSING_FIELDS);
    }

  } catch (error) {
    httpResp.error(res, app.httpResponses.errorCode.INTERNAL_SERVER_ERROR,
      `${FILE_LOG} (deleteValues): error (${error.stack})`);
  }
}


/**
 * Función para eliminar una coleccion
 *
 * @param req   Request
 * @param res   Response
 */
export async function drop(req: Request, res: Response) {
  
  try {

    const { collection } = req.params;

    if (collection) {

      // Ingreso nuevos valores en la base
      const result = await mongodb.drop(collection);

      httpResp.success(res, result);
    } else {
      console.log(`Parametros coleccion y valor incompletos`);
      httpResp.error(res, app.httpResponses.errorCode.BAD_REQUEST, app.httpResponses.errorMsgs.MISSING_FIELDS);
    }

  } catch (error) {
    httpResp.error(res, app.httpResponses.errorCode.INTERNAL_SERVER_ERROR,
      `${FILE_LOG} (drop): error (${error.stack})`);
  }
}

/**
 * Función para crear una coleccion
 *
 * @param req   Request
 * @param res   Response
 */
export async function create(req: Request, res: Response) {
  
  try {

    const { collection } = req.params;

    console.log(req.params)

    if (collection) {

      // Ingreso nuevos valores en la base
      const result = await mongodb.create(collection);

      httpResp.success(res, result);
    } else {
      console.log(`Se debe ingresar nombre de coleccion a crear`);
      httpResp.error(res, app.httpResponses.errorCode.BAD_REQUEST, app.httpResponses.errorMsgs.MISSING_FIELDS);
    }

  } catch (error) {
    httpResp.error(res, app.httpResponses.errorCode.INTERNAL_SERVER_ERROR,
      `${FILE_LOG} (create): error (${error.stack})`);
  }
}

# MongoDB with Node

Proyecto de ejemplo practico de una app de node comunicandose con una base de datos nosql de mongoDB.
Se utiliza el driver mongoDB, sinedo este el oficial por parte de MongoDB.

https://www.npmjs.com/package/mongodb

## Cuando usar una base de datos noSQL?

La gran mayoria de los sistemas usan base de datos SQL, es decir, del tipo relacional donde la integridad de los datos es la prioridad, por ejemplo en sistemas bancarios y empresariales.

Por otro lado si la consistencia no es una prioridad pero si lo es la velocidad de lectura, ahi es donde las bases noSQL sacan beneficio, por ejemplo en las redes sociales donde hay muchisimas mas lecturas que escrituras.

## Instalacion y configuracion

Para instalar la app se debe clonar el repositorio

```
git clone git@gitlab.com:nicogena90/mongodb-with-node.git
```

Una vez descargado se debe instalar las dependencias con:

```
npm i
```

Para configurar la app de deben completar las siguientes variables en el archivo mongodb.ts o las varibales de entorno:

```
const _DBUSER_ = process.env.dbuser || [Nombre de usuario para conectarse a la base];
const _DBPASSWORD_ = process.env.dbpassword || [Constraseña del usuario para conectarse a la base];
const _DBNAME_ = process.env.dbname || [Nombre de la base de datos a conectar];
const _HOST_ = process.env.dbhost || [Host del cluster donde se encuentra la base];
```

Una vez que se configuran las variables se inicia la app con

```
npm run start-dev
```

## EndPoints

Aca se explican los diferentes endpoint:

### Para crear una coleccion se usa

Method POST

http://localhost:5000/collection/[NombreColeccion]

### Para agregar items a una coleccion se usa

Method POST

http://localhost:5000/collection/

```
body: 
{
  "collection": "Personas",
  "value": [{
    "id": 1,
    "name": "Nicolas"
  }]
}
```

### Para obtener items de una coleccion se usa

Method GET

http://localhost:5000/collection/[NombreColeccion]

### Para obtener items de una coleccion por parametro se usa

Method GET

http://localhost:5000/collection/[NombreColeccion]/id/[Id]

En el ejemplo solo se puede buscar por id pero mongodb acepta busquedas por varios parametros.

Tambien estan los parametros de SORT y LIMIT dentro de las funciones, en el saco de sort tiene el siguiente formato:

```
{ name: 1 } // ascending
{ name: -1 } // descending
```

Es decir se elige la propiedad por la que se quiere ordenar y se le da el valor 1 o -1 segun lo que se quiera hacer

El Limit define la cantidad de items que se quiere traer de la coleccion, sinedo por default 0 que corresponde a no limite

### Para eliminar una coleccion se usa

Method DELETE

http://localhost:5000/collection/[NombreColeccion]

### Para eliminar items de una coleccion se usa

Method DELETE

http://localhost:5000/

```
body: 
{
  "collection": "Personas",
  "value": {
    "id": 1
  }
}
```

## Fuentes:

- https://medium.com/@rossbulat/using-promises-async-await-with-mongodb-613ed8243900
- https://www.w3schools.com/nodejs/nodejs_mongodb.asp
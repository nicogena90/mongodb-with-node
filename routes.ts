import { Router } from "express";
import * as controller from "./controllers/data";

const api: Router = Router();

api.get("/collection/:collection",          controller.get);
api.get("/collection/:collection/id/:id",   controller.getById);
api.post("/",                               controller.add);
api.post("/collection/:collection",         controller.create);
api.delete("/",                             controller.deleteValues);
api.delete("/collection/:collection",       controller.drop);

export default api;
// Componentes
const MongoClient = require('mongodb').MongoClient;
const assert = require('assert');

// Constantes de configuración
const _DBUSER_ = process.env.dbuser || "[USER]";
const _DBPASSWORD_ = process.env.dbpassword || "[PASSWORD]";
const _DBNAME_ = process.env.dbname || "[DBNAME]]";
const _HOST_ = process.env.dbhost || "[HOST]";

// Propiedades del modulo
let _initialized_ = false;
let _pool_: any;

/**
 * Inicia el servicio de acceso a bases de datos
 *
 * @return              true o false
 *
 */
export async function start(): Promise<boolean> {

  let ret = false;

  // Si el modulo no fue inicializado
  if (_initialized_ === false) {

    try {

      // Se amra la url para conectar con la base de datos
      const url = `mongodb+srv://${_DBUSER_}:${_DBPASSWORD_}@${_HOST_}`;
 
      // Use connect method to connect to the server
      MongoClient.connect(url, function(err: any, client: any) {
      assert.equal(null, err);
      console.log("Connected successfully to server");
      
      _pool_ = client;

      // Define como inicializado al modulo
      _initialized_ = true;

      });

    } catch (err) {
      console.error(`db.ts (start): error (${err.stack})`);
    }

  } else {

    ret = true;
  }

  return ret;

}

/**
 * Se recibe una coleccion y se devuelven items de la coleccion
 *
 * @param collection               nombre de la coleccion
 * @param query                    json de items que se quieren traer
 * @param sort                     json de item por el cual se quiere ordenar
 * @param limit                    numero de items que se quieren traer
 *
 * @return                         resultados de las consultas o false
 *
 */
export async function select(collection: string, query?: any, sort?: any, limit = 0): Promise<any> {

  let results: any = false;

  try {

    // Inicializa la conexion con la base de datos
    start();

    // Creamos la promesa para traer los datos de la base
    var myPromise = () => {
      return new Promise((resolve, reject) => {

        const db = _pool_.db(_DBNAME_);
        
          db
          .collection(collection)
          .find(query)
          .sort(sort)
          .limit(limit)
          .toArray(function(err: any, data: any) {
            err 
              ? reject(err) 
              : resolve(data);
          });
      });
    };

    results = await myPromise();

  } catch (err) {
    throw err;
  }

  return results;

}

/**
 * Se insertan items en una coleccion
 *
 * @param collection              coleccion a insertar
 * @param value                   items a insertar
 *
 * @return                        resultados de las consultas o false
 *
 */
export async function insert(collection: string, value: any): Promise<any> {

  let results: any = false;

  try {

    // Inicializa la conexion con la base de datos
    start();

    // Creamos la promesa para traer los datos de la base
    var myPromise = () => {
      return new Promise((resolve, reject) => {

        const db = _pool_.db(_DBNAME_);
        
        db.
        collection(collection)
        .insertMany(value, function(err: any, data: any) {
          err 
          ? reject(err) 
          : resolve(data.ops);;
        })

      });
    };

    results = await myPromise();  

  } catch (err) {
    throw err;
  }

  return results;

}

/**
 * 
 * Se eliminan items de una coleccion
 *
 * @param collection              coleccion a eliminar items
 * @param value                   items a eliminar
 *
 * @return                        resultados de las consultas o false
 *
 */
export async function deleteValues(collection: string, value: any): Promise<any> {

  let results: any = false;

  try {

    // Inicializa la conexion con la base de datos
    start();

    // Creamos la promesa para traer los datos de la base
    var myPromise = () => {
      return new Promise((resolve, reject) => {

        const db = _pool_.db(_DBNAME_);
        
        db.
        collection(collection)
        .deleteMany(value, function(err: any, data: any) {
          err 
          ? reject(err) 
          : resolve(data.ops);;
        })

      });
    };

    results = await myPromise();  

  } catch (err) {
    throw err;
  }

  return results;

}

/**
 * 
 * Se crea una coleccion en la base de datos
 *
 * @param collection              coleccion a crear
 *
 * @return                        resultados de las consultas o false
 *
 */
export async function create(collection: string): Promise<any> {

  let results: any = false;

  try {

    // Inicializa la conexion con la base de datos
    start();

    // Creamos la promesa para traer los datos de la base
    var myPromise = () => {
      return new Promise((resolve, reject) => {

        const db = _pool_.db(_DBNAME_);
        
        db
        .createCollection(collection, function(err: any, data: any) {
          err 
          ? reject(err) 
          : resolve(data.ops);;
        })

      });
    };

    results = await myPromise();  

  } catch (err) {
    throw err;
  }

  return results;

}

/**
 * 
 * Se elimina una coleccion de la base de datos
 *
 * @param collection              coleccion a eliminar
 *
 * @return                        resultados de las consultas o false
 *
 */
export async function drop(collection: string): Promise<any> {

  let results: any = false;

  try {

    // Inicializa la conexion con la base de datos
    start();

    // Creamos la promesa para traer los datos de la base
    var myPromise = () => {
      return new Promise((resolve, reject) => {

        const db = _pool_.db(_DBNAME_);
        
        db
        .collection(collection)
        .drop(function(err: any, data: any) {
          err 
          ? reject(err) 
          : resolve(data.ops);;
        })

      });
    };

    results = await myPromise();  

  } catch (err) {
    throw err;
  }

  return results;

}
